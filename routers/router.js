const express = require('express');
const router = new express.Router();
const multer = require('multer');

router.get('/', async (req, res) => {
	try {
		res.send('Server pinged!');
	} catch (error) {
		res.status(404).send(error.message);
	}
});
router.get('/user/:id', async (req, res) => {
	const id = req.params.id;
	try {
		res.json(id);
	} catch (error) {
		res.status(404).send(error.message);
	}
});
router.get('/getName', async (req, res) => {
	const data = req.query;
	try {
		res.send(data);
	} catch (error) {
		res.status(404).send(error.message);
	}
});
router.post('/getData', multer().none(), async (req, res) => {
	const data = req.body;
	try {
		res.send(data);
	} catch (error) {
		res.status(404).send(error.message);
	}
});
router.get('*', async (req, res) => {
	try {
		res.send('Route not found!');
	} catch (error) {
		res.status(404).send(error.message);
	}
});
module.exports = router;
